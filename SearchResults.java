import javax.swing.*;

public class SearchResults {
    public static void main(String[] args) {
        SearchWordInFile searchWord = new SearchWordInFile();
        JFrame filenameFrame = new JFrame();
        String filename = JOptionPane.showInputDialog(filenameFrame,"Enter your filepath: ");
        JFrame wordFrame = new JFrame();
        String word = JOptionPane.showInputDialog(wordFrame,"Type the word you wish to search: ");
        searchWord.findWordInFile(filename, word);
        System.exit(0);

    }
}
