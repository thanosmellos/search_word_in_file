import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MSWordTextFinder {
    public void findTextInWordDocument(String filename, String... words) throws IOException {
        try {
            File file = new File(filename);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            XWPFWordExtractor ex = new XWPFWordExtractor(document);
            String text = ex.getText();
            for (String word : words) {
                if (text.toLowerCase().contains(word.toLowerCase())) {
                    System.out.println("Word " + word + " is contained in the document");
                }
                else{
                    System.out.println("Word "+word+" is not contained in the document");
                }
            }
        }catch(IOException e){
            System.out.println("file not found");
        }


    }

}
