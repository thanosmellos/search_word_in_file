import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FindWordInTextFile {
    public void findSingleWordInTextDocument(String filename, String arg) throws IOException {
        int count = 0,countBuffer=0,countLine=0;
        String lineNumber = "";
        String line;

        try {

            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            try {
                while((line = br.readLine()) != null){

                    countLine++;
                    String[] txtwords = line.split(" ");

                    for (String str : txtwords) {

                        if (str.toLowerCase().contains(arg.toLowerCase())) {
                            count++;
                            countBuffer++;
                        }
                    }

                    if(countBuffer > 0)
                    {
                        countBuffer = 0;
                        lineNumber = lineNumber + countLine + ",";
                    }

                }
                br.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        System.out.println("Word "+arg+" was found "+count+" times in the text");
        System.out.println("Word was found at lines: "+lineNumber);

    }

    public void findMultipleWordsInTextDocument(String filename, String... args) throws IOException {

        for(String arg : args){
            findSingleWordInTextDocument(filename,arg);
        }
    }
}
