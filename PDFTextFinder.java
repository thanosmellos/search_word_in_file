import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class PDFTextFinder {
    public void findTextInPDFDocument (String filepath, String... args) throws IOException{
        try {
            File file = new File(filepath);
            PDDocument document = PDDocument.load(file);

                Splitter splitter = new Splitter();
                List<PDDocument> Pages = splitter.split(document);
                Iterator<PDDocument> iterator = Pages.listIterator();

                while (iterator.hasNext()) {
                    PDDocument pd = iterator.next();
                    PDFTextStripper textStripper = new PDFTextStripper();
                    for (String word : args) {
                        if (textStripper.getText(pd).toLowerCase().contains(word.toLowerCase()) == true) {
                            System.out.println("Word: " + word + ", Found: Yes, Page " + ((ListIterator<PDDocument>) iterator).nextIndex());
                        }
                    }
                }
        }catch(FileNotFoundException e) {

            e.printStackTrace();
        }
    }
}
