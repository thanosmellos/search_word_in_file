import java.io.IOException;

public class SearchWordInFile {
    public SearchWordInFile(){}
    public boolean pdfFiletype(String filepath){
        return filepath.endsWith("pdf");
    }

    public boolean docxFiletype(String filepath){
        return filepath.endsWith("docx");
    }

    public boolean docFiletype(String filepath){
        return filepath.endsWith("docx");
    }

    public boolean textFiletype(String filepath){
        return filepath.endsWith("txt");
    }

    public void findWordInFile(String filepath, String... words){
        try {
        if(pdfFiletype(filepath)){
            PDFTextFinder pdfFinder = new PDFTextFinder();
                pdfFinder.findTextInPDFDocument(filepath, words);
            }else if(docFiletype(filepath) || docxFiletype(filepath)){
            MSWordTextFinder docFinder = new MSWordTextFinder();
            docFinder.findTextInWordDocument(filepath,words);
        }else if(textFiletype(filepath)){
            FindWordInTextFile textFinder = new FindWordInTextFile();
            textFinder.findMultipleWordsInTextDocument(filepath, words);
        }else{
            System.out.println("File not PDF, DOC or TEXT type");
        }
            }catch(IOException e) {
            e.printStackTrace();
            }
    }
}
